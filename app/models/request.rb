class Request < ApplicationRecord
  belongs_to :user
  belongs_to :category

  has_one_attached :attachement_files

  validates :title, presence: {message: "Cannot be Empty"}
  validates :description, presence: {message: "Cannot be Empty"}
  validates :delivery, numericality: {only_integer: true, message: "Must be a number"}
end
